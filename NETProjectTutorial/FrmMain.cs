﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;
        private ProductoModel productoModel;
        private EmpleadoModel empleadoModel;
        private ClienteModel clienteModel;

        public FrmMain()
        {
            InitializeComponent();
            productoModel = new ProductoModel();
            empleadoModel = new EmpleadoModel();
            clienteModel = new ClienteModel();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

            List<Producto> productos = productoModel.GetProductos();
            if(productos.Count == 0)
            {
                productoModel.Populate();
                productos = productoModel.GetProductos();
            }
            foreach (Producto p in productos)
            {
                DataRow drProducto = dsProductos.Tables["Producto"].NewRow();
                drProducto["Id"] = p.Id;
                drProducto["Sku"] = p.Sku;
                drProducto["Nombre"] = p.Nombre;
                drProducto["Descripcion"] = p.Descripcion;
                drProducto["Cantidad"] = p.Cantidad;
                drProducto["Precio"] = p.Precio;
                drProducto["SKUN"] = p.Sku + " " + p.Nombre;

                dsProductos.Tables["Producto"].Rows.Add(drProducto);
                drProducto.AcceptChanges();
            }

            List<Empleado> empleados = empleadoModel.GetListEmpleado();
            if (empleados.Count == 0)
            {
                empleadoModel.Populate();
                empleados = empleadoModel.GetListEmpleado();
            }
            foreach (Empleado em in empleados)
            {
                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();
                drEmpleado["Id"] = em.Id;
                drEmpleado["Cedula"] = em.Cedula;
                drEmpleado["Nombres"] = em.Nombre;
                drEmpleado["Apellidos"] = em.Apellidos;
                drEmpleado["Telefono"] = em.Tconvencional;
                drEmpleado["Celular"] = em.Tcelular;
                drEmpleado["Salario"] = em.Salario;
                drEmpleado["Direccion"] = em.Direccion;
                drEmpleado["Sexo"] = em.Sexo.ToString();
                drEmpleado["INSS"] = em.Inss;
                drEmpleado["NA"] = em.Nombre + " " + em.Apellidos;

                dsProductos.Tables["Empleado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();
            }

            List<Cliente> clientes = clienteModel.GetListCliente();
            if (clientes.Count == 0)
            {
                clienteModel.Populate();
                clientes = clienteModel.GetListCliente();
            }
            foreach (Cliente c in clientes)
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = c.Id;
                drCliente["Cedula"] = c.Cedula;
                drCliente["Nombres"] = c.Nombres;
                drCliente["Apellidos"] = c.Apellidos;
                drCliente["Telefono"] = c.Telefono;
                drCliente["Correo"] = c.Correo;
                drCliente["Direccion"] = c.Direccion;
                drCliente["NA"] = c.Nombres + " " + c.Apellidos;

                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }

            foreach (Factura f in new FacturaModel().GetListFactura())
            {
                DataRow drFactura = dsProductos.Tables["Factura"].NewRow();
                drFactura["Id"] = f.Id;
                drFactura["CodFactura"] = f.Cod_factura;
                drFactura["Empleado"] = f.Empleado.Id;
                drFactura["Cliente"] = f.Cliente.Id;
                drFactura["Fecha"] = f.Fecha;
                drFactura["Observaciones"] = f.Observaciones;
                drFactura["Subtotal"] = f.Subtotal;
                drFactura["Iva"] = f.Iva;
                drFactura["Total"] = f.Total;

                dsProductos.Tables["Factura"].Rows.Add(drFactura);
                drFactura.AcceptChanges();
            }

            foreach (DetalleFactura df in new DetalleFacturaModel().GetListDetalleFactura())
            {
                DataRow drDetalleFactura = dsProductos.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Id"] = df.Id;
                drDetalleFactura["Factura"] = df.Factura.Id;
                drDetalleFactura["Producto"] = df.Producto.Id;
                drDetalleFactura["Cantidad"] = df.Cantidad;
                drDetalleFactura["Precio"] = df.Precio;

                dsProductos.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
                drDetalleFactura.AcceptChanges();
            }

        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }

        private void FacturasEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFacturasEmpleados ffm = new FrmFacturasEmpleados();
            ffm.MdiParent = this;
            ffm.DsFactura = dsProductos;
            ffm.Show();
        }
    }
}
