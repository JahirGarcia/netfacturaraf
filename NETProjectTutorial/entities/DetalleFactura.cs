﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class DetalleFactura
    {
        private int id;//4
        private Factura factura;//4
        private Producto producto;//4
        private int cantidad;//4
        private double precio;//8
        //Total => 24

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal Factura Factura
        {
            get
            {
                return factura;
            }

            set
            {
                factura = value;
            }
        }

        internal Producto Producto
        {
            get
            {
                return producto;
            }

            set
            {
                producto = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public DetalleFactura(int id, Factura factura, Producto producto, int cantidad, double precio)
        {
            this.Id = id;
            this.Factura = factura;
            this.Producto = producto;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

        public DetalleFactura() { }
    }
}
