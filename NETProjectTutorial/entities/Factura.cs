﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;//4
        private string cod_factura;//8 => 19
        private DateTime fecha;//20 => 43
        private Empleado empleado;//4
        private Cliente cliente;//4
        private string observaciones;//100 => 203
        private double subtotal;//8
        private double iva;//8
        private double total;//8
        //Total => 297
        public Factura() { }

        public Factura(int id, string cod_factura, DateTime fecha, Empleado empleado, Cliente cliente, string observaciones, double subtotal, double iva, double total)
        {
            this.Id = id;
            this.Cod_factura = cod_factura;
            this.Fecha = fecha;
            this.Empleado = empleado;
            this.Cliente = cliente;
            this.Observaciones = observaciones;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
        }

        public int Id { get => id; set => id = value; }
        public string Cod_factura { get => cod_factura; set => cod_factura = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Observaciones { get => observaciones; set => observaciones = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Iva { get => iva; set => iva = value; }
        public double Total { get => total; set => total = value; }
        internal Empleado Empleado { get => empleado; set => empleado = value; }
        internal Cliente Cliente { get => cliente; set => cliente = value; }
    }
}
