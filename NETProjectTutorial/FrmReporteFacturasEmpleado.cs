﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFacturasEmpleado : Form
    {
        private DataSet dsSistema;
        private DataRow drEmpleado;
        public FrmReporteFacturasEmpleado()
        {
            InitializeComponent();
        }

        public DataSet DsSistema { get => dsSistema; set => dsSistema = value; }
        public DataRow DrEmpleado { get => drEmpleado; set => drEmpleado = value; }

        private void FrmReporteFacturasEmpleado_Load(object sender, EventArgs e)
        {
            DataTable dtFacturas = dsSistema.Tables["Factura"];

            DataRow[] drFacturasEmpleado = dtFacturas.Select(string.Format("Empleado = {0}", drEmpleado["Id"]));

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.ReporteFacturasEmpleado.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsFactura", drFacturasEmpleado);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);

            this.reportViewer1.RefreshReport();
        }
    }
}
