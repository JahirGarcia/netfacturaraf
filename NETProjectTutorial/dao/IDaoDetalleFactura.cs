﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NETProjectTutorial.dao
{
    interface IDaoDetalleFactura : IDao<DetalleFactura>
    {
        DetalleFactura findById(int id);
    }
}