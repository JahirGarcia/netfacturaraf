﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NETProjectTutorial.dao
{
    interface IDaoFactura : IDao<Factura>
    {
        Factura findById(int id);
    }
}