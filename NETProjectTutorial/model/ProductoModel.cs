﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static List<Producto> productos = new List<Producto>();
        private implements.DaoImplementsProducto daoproducto;

        public ProductoModel()
        {
            daoproducto = new DaoImplementsProducto();
        }

        public List<Producto> GetProductos()
        {
            return daoproducto.findAll();
        }

        public void Populate()
        {
            productos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach (Producto e in productos)
            {
                daoproducto.save(e);
            }
        }

        public void save(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["Sku"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripcion"].ToString();
            p.Cantidad = int.Parse(producto["Cantidad"].ToString());
            p.Precio = double.Parse(producto["Precio"].ToString());

            daoproducto.save(p);

        }

        public void update(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["Sku"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripcion"].ToString();
            p.Cantidad = int.Parse(producto["Cantidad"].ToString());
            p.Precio = double.Parse(producto["Precio"].ToString());

            daoproducto.update(p);
        }

        public Producto findById(int id)
        {
            return daoproducto.findById(id);
        }
    }
}
