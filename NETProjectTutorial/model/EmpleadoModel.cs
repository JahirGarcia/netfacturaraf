﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.implements;
using System.Data;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleados = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoempleado;

        public EmpleadoModel()
        {
            daoempleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoempleado.findAll();
        }

        public void Populate()
        {
            ListEmpleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));
            foreach (Empleado e in ListEmpleados)
            {
                daoempleado.save(e);
            }
        }

        public void save(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Cedula = empleado["Cedula"].ToString();
            e.Inss = empleado["Inss"].ToString();
            e.Nombre = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Tconvencional = empleado["Telefono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            e.Salario = double.Parse(empleado["Salario"].ToString());
            e.Direccion = empleado["Direccion"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString());

            daoempleado.save(e);
        }

        public void update(DataRow empleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empleado["Id"].ToString());
            e.Cedula = empleado["Cedula"].ToString();
            e.Inss = empleado["Inss"].ToString();
            e.Nombre = empleado["Nombres"].ToString();
            e.Apellidos = empleado["Apellidos"].ToString();
            e.Tconvencional = empleado["Telefono"].ToString();
            e.Tcelular = empleado["Celular"].ToString();
            e.Salario = double.Parse(empleado["Salario"].ToString());
            e.Direccion = empleado["Direccion"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empleado["Sexo"].ToString());

            daoempleado.update(e);
        }

        public Empleado findById(int id)
        {
            return daoempleado.findById(id);
        }
    }
}
