﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFacturasEmpleados : Form
    {
        private DataSet dsFactura;
        private BindingSource bsFactura;

        public FrmFacturasEmpleados()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
        }

        public DataSet DsFactura { get => dsFactura; set => dsFactura = value; }

        private void FrmFacturasEmpleados_Load(object sender, EventArgs e)
        {
            // Agregando datos a la tabla
            bsFactura.DataSource = dsFactura;
            bsFactura.DataMember = dsFactura.Tables["Factura"].TableName;
            dgvFacturasEmpleado.DataSource = bsFactura;
            dgvFacturasEmpleado.AutoGenerateColumns = true;

            // Agregando datos al combo box
            cmbEmpleado.DisplayMember = "NA";
            cmbEmpleado.ValueMember = "Id";
            cmbEmpleado.DataSource = dsFactura.Tables["Empleado"];
        }

        private void CmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Filtrando la tabla
            bsFactura.Filter = string.Format("Empleado = {0}", cmbEmpleado.SelectedValue);
        }

        private void BtnReporte_Click(object sender, EventArgs e)
        {
            FrmReporteFacturasEmpleado frfe = new FrmReporteFacturasEmpleado();
            frfe.DsSistema = dsFactura;
            frfe.DrEmpleado = ((DataRowView)cmbEmpleado.SelectedItem).Row;
            frfe.Show();
        }
    }
}
