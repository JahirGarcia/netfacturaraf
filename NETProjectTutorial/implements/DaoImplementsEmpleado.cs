﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : DaoImplements, IDaoEmpleado
    {
        private const string FILENAME_HEADER = "hempleado.dat";
        private const string FILENAME_DATA = "dempleado.dat";
        private const int SIZE = 410;

        public DaoImplementsEmpleado() { }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            List<Empleado> empleados = new List<Empleado>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string cedula = brdata.ReadString();
                string inss = brdata.ReadString();
                string nombres = brdata.ReadString();
                string apellidos = brdata.ReadString();
                double salario = brdata.ReadDouble();
                string direccion = brdata.ReadString();
                string tconvencional = brdata.ReadString();
                string tcelular = brdata.ReadString();
                Empleado.SEXO sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), brdata.ReadString());
                Empleado e = new Empleado(id, nombres, apellidos, cedula, inss, direccion, salario, tconvencional, tcelular, sexo);
                empleados.Add(e);
            }

            close();
            return empleados;
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Empleado findById(int id)
        {
            Empleado e = null;
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int epos;
            for (epos = 0; epos < n; epos++)
            {
                int eid = brheader.ReadInt32();
                if (eid == id)
                {
                    long dpos = epos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string cedula = brdata.ReadString();
                    string inss = brdata.ReadString();
                    string nombres = brdata.ReadString();
                    string apellidos = brdata.ReadString();
                    double salario = brdata.ReadDouble();
                    string direccion = brdata.ReadString();
                    string tconvencional = brdata.ReadString();
                    string tcelular = brdata.ReadString();
                    Empleado.SEXO sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), brdata.ReadString());
                    e = new Empleado(id, nombres, apellidos, cedula, inss, direccion, salario, tconvencional, tcelular, sexo);

                    break;
                }
            }
            close();

            return e;
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(t.Cedula);
            bwdata.Write(t.Inss);
            bwdata.Write(t.Nombre);
            bwdata.Write(t.Apellidos);
            bwdata.Write(t.Salario);
            bwdata.Write(t.Direccion);
            bwdata.Write(t.Tconvencional);
            bwdata.Write(t.Tcelular);
            bwdata.Write(t.Sexo.ToString());

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            close();
        }

        public int update(Empleado t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(t.Id);
                    bwdata.Write(t.Cedula);
                    bwdata.Write(t.Inss);
                    bwdata.Write(t.Nombre);
                    bwdata.Write(t.Apellidos);
                    bwdata.Write(t.Salario);
                    bwdata.Write(t.Direccion);
                    bwdata.Write(t.Tconvencional);
                    bwdata.Write(t.Tcelular);
                    bwdata.Write(t.Sexo.ToString());
                    close();

                    return t.Id;
                }
            }
            return -1;
        }
    }
}
