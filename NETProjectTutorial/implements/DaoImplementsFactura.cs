﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NETProjectTutorial.implements
{
    class DaoImplementsFactura : DaoImplements, IDaoFactura
    {
        private const string FILENAME_HEADER = "hfactura.dat";
        private const string FILENAME_DATA = "dfactura.dat";
        private const int SIZE = 297;

        public DaoImplementsFactura() { }
        public bool delete(Factura t)
        {
            throw new NotImplementedException();
        }

        public List<Factura> findAll()
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            List<Factura> facturas = new List<Factura>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string cod_factura = brdata.ReadString();
                DateTime fecha = DateTime.Parse(brdata.ReadString());
                int empleadoId = brdata.ReadInt32();
                Empleado empleado = new DaoImplementsEmpleado().findById(empleadoId);
                int clineteId = brdata.ReadInt32();
                Cliente cliente = new DaoImplementsCliente().findById(clineteId);
                string observaciones = brdata.ReadString();
                double subtotal = brdata.ReadDouble();
                double iva = brdata.ReadDouble();
                double total = brdata.ReadDouble();
                Factura f = new Factura(id, cod_factura, fecha, empleado, cliente, observaciones, subtotal, iva, total);
                facturas.Add(f);

            }

            close();
            return facturas;
        }

        public Factura findById(int id)
        {
            Factura f = null;
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int fpos;
            for (fpos = 0; fpos < n; fpos++)
            {
                int fid = brheader.ReadInt32();
                if (fid == id)
                {
                    long dpos = fpos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string cod_factura = brdata.ReadString();
                    DateTime fecha = DateTime.Parse(brdata.ReadString());
                    int empleadoId = brdata.ReadInt32();
                    Empleado empleado = new DaoImplementsEmpleado().findById(empleadoId);
                    int clineteId = brdata.ReadInt32();
                    Cliente cliente = new DaoImplementsCliente().findById(clineteId);
                    string observaciones = brdata.ReadString();
                    double subtotal = brdata.ReadDouble();
                    double iva = brdata.ReadDouble();
                    double total = brdata.ReadDouble();
                    f = new Factura(id, cod_factura, fecha, empleado, cliente, observaciones, subtotal, iva, total);

                    break;
                }
            }
            close();

            return f;
        }

        public void save(Factura t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(t.Cod_factura);
            bwdata.Write(t.Fecha.ToString());
            bwdata.Write(t.Empleado.Id);
            bwdata.Write(t.Cliente.Id);
            bwdata.Write(t.Observaciones);
            bwdata.Write(t.Subtotal);
            bwdata.Write(t.Iva);
            bwdata.Write(t.Total);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            close();
        }

        public int update(Factura t)
        {
            throw new NotImplementedException();
        }
    }
}