﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NETProjectTutorial.implements
{
    class DaoImplementsDetalleFactura : DaoImplements, IDaoDetalleFactura
    {
        private const string FILENAME_HEADER = "hdetallefactura.dat";
        private const string FILENAME_DATA = "ddetallefactura.dat";
        private const int SIZE = 24;

        public DaoImplementsDetalleFactura() { }
        public bool delete(DetalleFactura t)
        {
            throw new NotImplementedException();
        }

        public List<DetalleFactura> findAll()
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            List<DetalleFactura> detalleFacturas = new List<DetalleFactura>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                int idFactura = brdata.ReadInt32();
                Factura factura = new DaoImplementsFactura().findById(idFactura);
                int idProducto = brdata.ReadInt32();
                Producto producto = new DaoImplementsProducto().findById(idProducto);
                int cantidad = brdata.ReadInt32();
                double precio = brdata.ReadDouble();
                DetalleFactura df = new DetalleFactura(id, factura, producto, cantidad, precio);
                detalleFacturas.Add(df);
            }

            close();
            return detalleFacturas;
        }

        public DetalleFactura findById(int id)
        {
            throw new NotImplementedException();
        }

        public void save(DetalleFactura t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(t.Factura.Id);
            bwdata.Write(t.Producto.Id);
            bwdata.Write(t.Cantidad);
            bwdata.Write(t.Precio);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            close();
        }

        public int update(DetalleFactura t)
        {
            throw new NotImplementedException();
        }
    }
}