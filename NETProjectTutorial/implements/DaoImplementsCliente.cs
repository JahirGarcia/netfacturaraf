﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsCliente : DaoImplements, IDaoCliente
    {
        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int SIZE = 390;

        public DaoImplementsCliente() { }

        public bool delete(Cliente t)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findAll()
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            List<Cliente> clientes = new List<Cliente>();

            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brheader.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdata.ReadInt32();
                string cedula = brdata.ReadString();
                string nombres = brdata.ReadString();
                string apellidos = brdata.ReadString();
                string telefono = brdata.ReadString();
                string correo = brdata.ReadString();
                string direccion = brdata.ReadString();
                Cliente c = new Cliente(id, cedula, nombres,
                    apellidos, telefono, correo, direccion);
                clientes.Add(c);
            }

            close();
            return clientes;
        }

        public Cliente findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Cliente findById(int id)
        {
            Cliente c = null;
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int cid = brheader.ReadInt32();
                if (cid == id)
                {
                    long dpos = cpos * SIZE;
                    brdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    brdata.ReadInt32();//lee el id
                    string cedula = brdata.ReadString();
                    string nombres = brdata.ReadString();
                    string apellidos = brdata.ReadString();
                    string telefono = brdata.ReadString();
                    string correo = brdata.ReadString();
                    string direccion = brdata.ReadString();
                    c = new Cliente(id, cedula, nombres,
                        apellidos, telefono, correo, direccion);

                    break;
                }
            }
            close();

            return c;
        }

        public List<Cliente> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Cliente t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            long dpos = k * SIZE;
            bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdata.Write(++k);
            bwdata.Write(t.Cedula);
            bwdata.Write(t.Nombres);
            bwdata.Write(t.Apellidos);
            bwdata.Write(t.Telefono);
            bwdata.Write(t.Correo);
            bwdata.Write(t.Direccion);

            bwheader.BaseStream.Seek(0, SeekOrigin.Begin);
            bwheader.Write(++n);
            bwheader.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwheader.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwheader.Write(k);
            close();
        }

        public int update(Cliente t)
        {
            open(FILENAME_HEADER, FILENAME_DATA);
            brheader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brheader.ReadInt32();
            int k = brheader.ReadInt32();

            int cpos;
            for (cpos = 0; cpos < n; cpos++)
            {
                int id = brheader.ReadInt32();
                if (id == t.Id)
                {
                    long dpos = cpos * SIZE;
                    bwdata.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    bwdata.Write(t.Id);
                    bwdata.Write(t.Cedula);
                    bwdata.Write(t.Nombres);
                    bwdata.Write(t.Apellidos);
                    bwdata.Write(t.Telefono);
                    bwdata.Write(t.Correo);
                    bwdata.Write(t.Direccion);
                    close();

                    return t.Id;
                }
            }
            return -1;
        }
    }
}
